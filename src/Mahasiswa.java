
public class Mahasiswa {
	
	String nrp;
	String name;
	double ipk;
	
	public Mahasiswa() {
	}
	
	public String getNrp() {
		return nrp;
	}
	public void setNrp(String nrp) {
		this.nrp = nrp;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getIpk() {
		return ipk;
	}
	public void setIpk(double ipk) {
		this.ipk = ipk;
	}
	
	public boolean cekIpk(double ipk){
		if (ipk == 4.00) {
			return true;
		}else if(ipk == 3.00){
			return true;
		}else if(ipk == 2.00){
			return true;
		}else if(ipk == 1.00){
			return true;
		}else{
			return false;
		}
	}
	
	
}
