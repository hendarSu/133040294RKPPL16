import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;



public class MahasiswaTest {
	
	Mahasiswa mhs =new Mahasiswa();
	
	@After
	public void test1(){
		mhs.setName("Hendar");
		System.out.println(mhs.getName());
	}
	
	@Test
	public void test2(){
		mhs.setName(null);
		assertNull("seharusnya null", mhs.getName());
	}
	
	@Test
	public void test21(){
		mhs.setName("apalah");
		assertNull("seharusnya null", mhs.getName());
	}
	
	
	@Test
	public void test3(){
		mhs.setName("Hendar");
		assertNotNull("Seharusnya tidak null",mhs.getName());
	}
	
	@Test
	public void test31(){
		mhs.setName(null);
		assertNotNull("Seharusnya tidak null",mhs.getName());
	}
	
	@Test
	public void test4(){
		assertTrue("seharusnya true", mhs.cekIpk(4.00));
	}
	
	@Test
	public void test5(){
		assertTrue("seharusnya true", mhs.cekIpk(-4.00));
	}
	
	@Test
	public void test6(){
		assertFalse("seharusnya false", mhs.cekIpk(-4.00));
	}
	
	@Test
	public void test7(){
		assertFalse("seharusnya false", mhs.cekIpk(3.00));
	}
	
	@Before
	public void test8(){
		System.out.println("Test selesai");
	}
	
	
}
